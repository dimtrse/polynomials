package com.ds;

public class Polynomial {
    private final double[] data;
    private final StringBuilder expression;
    private double result = 0.00;


    public Polynomial(double[] data) {
        this.data = data;
        for (double item : data) {
            if (item == 0){
                throw new ArithmeticException("Cannot be divided by zero");
            }
        }
        expression = new StringBuilder();
    }

    public void run() {
        for (int i = 1; i < data.length; i++) {
            double item = data[i];
            expression.append("1/").append(item).append("*3");
            result += 1 / item * 3;

            if (i != data.length - 1)
                expression.append(" + ");
        }
    }

    public double[] getData() {
        return data;
    }

    public StringBuilder getExpression() {
        return expression;
    }

    public double getResult() {
        return result;
    }
}
