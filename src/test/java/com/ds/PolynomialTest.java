package com.ds;


import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PolynomialTest {

    @Test
    void run() {
        double[] data = {2, 3, 4, 1, 2, 3, 41, 2, 3, 4, 23, 2};
        Polynomial polynomial = new Polynomial(data);
        polynomial.run();

        Assert.assertEquals(polynomial.getResult(), 12.203, 0.1);

        data = new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        double[] finalData = data;
        Assertions.assertThrows(ArithmeticException.class, () -> new Polynomial(finalData));

        data = new double[]{-1, 2, -52, 4, -2, 2, -1, 1};
        polynomial = new Polynomial(data);
        polynomial.run();

        Assert.assertEquals(polynomial.getResult(), 2.2, 0.1);
    }
}